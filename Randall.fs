﻿open System.Threading.Tasks
open DSharpPlus
open DSharpPlus.CommandsNext
open randall.Robot

[<EntryPoint>]
let main args =
    printfn "Starting"

    let token = args[0]
    let config = DiscordConfiguration ()
    config.Token <- token
    config.TokenType <- TokenType.Bot

    let client = new DiscordClient(config)

    let commandsConfig = CommandsNextConfiguration ()
    commandsConfig.StringPrefixes <- ["!"]

    let commands = client.UseCommandsNext(commandsConfig)
    commands.RegisterCommands<BardBot>()

    client.ConnectAsync()
    |> Async.AwaitTask
    |> Async.RunSynchronously

    Task.Delay(-1)
    |> Async.AwaitTask
    |> Async.RunSynchronously

    1

