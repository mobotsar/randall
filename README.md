# Randall

The coolest RNJESUS around - successor of a long-dead predecessor.

### To Set Up

1. You must have a dotnet sdk installed. Obtain this from your distribution repositories on linux, or from [the dotnet website](https://dotnet.microsoft.com/en-us/download) if you're on Windows. If you use Mac or something else, figure it out.

2. Obtain a copy of Randall's source code. This may be done by either cloning the repository via a git client, or by downloading and extracting an archive file (select an archive format from the "Code" dropdown).

3. Obtain a discord bot token. The portal for this is found [here](https://discord.com/developers/applications).

5. Open a powershell/bash terminal, and execute `dotnet run TOKEN` from within the folder of the downloaded source code, where TOKEN is your actual discord bot token.

6. Add your very own Randall to whatever servers you please using an invite link generated from the OAuth2 tab in the discord developer portal.

### To Use

Basic syntax is `!r X`, where `X` is some dice expression.

Dice expressions come in a few forms.

1. Integers
2. Dice rolls
3. Math expressions

These three types can be nested, mutually, arbitrarily. For example, all of the following are valid.

```
!r 7            Just evaulates to 7.
!r 7 - 3        Evaluates to 4.
!r 7 - 3d17     Rolls 3d17, then subtracts their sum from seven.
!r d2           Rolls a d2.
!r 4d11g1       Rolls 4d11, keeping only the highest roll. Sums the kept dice.
!r 3d4l2        Rolls 3d4, keeping only the lowest two rolls. Sums the kept dice.
!r (4d2 + 5)d6  Rolls 4d2, adds 5 to the sum, then rolls that many d6 and sums them.
```
