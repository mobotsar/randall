module randall.Eval

open randall.Parser

let los xs = List.fold (fun a e -> a + (string e) + " ") "" xs

let dlim = 0

let eval p =

    let mutable depth = 0

    let mutable rolls_agg = ""

    let rec rpg0 (Rpg0.Single(rpg1_val, rpg01)) =
        let init = ref (rpg1 rpg1_val)
        traverse_rpg01 init rpg01
        init.Value
    and traverse_rpg01 init = function
        | Add(rpg1_val, rpg01) -> init.Value <- init.Value + (rpg1 rpg1_val); traverse_rpg01 init rpg01
        | Sub(rpg1_val, rpg01) -> init.Value <- init.Value - (rpg1 rpg1_val); traverse_rpg01 init rpg01
        | Rpg01.Nil -> ()

    and rpg1 (Rpg1.Single(rpg2_val, rpg11)) =
        let init = ref (rpg2 rpg2_val)
        traverse_rpg11 init rpg11
        init.Value
    and traverse_rpg11 init = function
        | Mul(rpg2_val, rpg11) -> init.Value <- init.Value * (rpg2 rpg2_val); traverse_rpg11 init rpg11
        | Div(rpg2_val, rpg11) -> init.Value <- init.Value / (rpg2 rpg2_val); traverse_rpg11 init rpg11
        | Rpg11.Nil -> ()

    and rpg2 (Rpg2.Single(all_val, rpg21)) =
        let init = ref (all all_val)
        traverse_rpg21 init rpg21
        init.Value
    and traverse_rpg21 init = function
        | Exp(all_val, rpg21) -> init.Value <- pown init.Value (all all_val); traverse_rpg21 init rpg21
        | Rpg21.Nil -> ()

    and all =
        function
        | All.Next gDice -> gdice gDice
        | All.A l0 ->
            let l0v = loop l0
            if l0v < 1 then failwith "Can't roll a die with fewer than one side"
            let roll = System.Random().Next(1, l0v + 1)
            if depth <= dlim then rolls_agg <- rolls_agg + $"d%d{l0v}: %i{roll}\n"
            roll
        | All.B (l0, l1) ->
            let l0v, l1v = loop l0, loop l1
            if l0v < 1 then failwith "Can't roll fewer than one die"
            if l1v < 1 then failwith "Can't roll a die with fewer than one side"
            let rolls = List.init l0v (fun _ -> System.Random().Next(1, l1v + 1)) |> List.sort
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}d%d{l1v}: %s{los rolls}\n"
            List.sum rolls

    and gdice = function
        | G_Dice.Next lDice -> ldice lDice
        | G_Dice.A l0 ->
            let l0v = loop l0
            if l0v < 1 then failwith "Can't roll a die with fewer than one side"
            let roll = System.Random().Next(1, l0v + 1)
            if depth <= dlim then rolls_agg <- rolls_agg + $"d%d{l0v}g: %i{roll}\n"
            roll
        | G_Dice.B (l0, l1) ->
            let l0v, l1v = loop l0, loop l1
            if l0v < 1 then failwith "Can't roll fewer than one die"
            if l1v < 1 then failwith "Can't roll a die with fewer than one side"
            let rolls = List.init l0v (fun _ -> System.Random().Next(1, l1v + 1)) |> List.sort
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}d%d{l1v}g: %s{los rolls}\n"
            rolls |> List.last
        | G_Dice.C (l0, l1) ->
            let l0v, l1v = loop l0, loop l1
            if l0v < 1 then failwith "Can't roll a die with fewer than one side"
            if l1v < 1 then failwith "Can't keep fewer than one die"
            let rolls = List.init 1 (fun _ -> System.Random().Next(1, l0v + 1))
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}g%d{l1v}: %s{los rolls}\n"
            rolls |> List.take l1v |> List.sum
        | G_Dice.D (l0, l1, l2) ->
            let l0v, l1v, l2v = loop l0, loop l1, loop l2
            if l0v < 1 then failwith "Can't roll fewer than one die"
            if l1v < 1 then failwith "Can't roll a die with fewer than one side"
            if l2v < 1 then failwith "Can't keep fewer than one die"
            let rolls = List.init l0v (fun _ -> System.Random().Next(1, l1v + 1)) |> List.sort
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}d%d{l1v}g%d{l2v}: %s{los rolls}\n"
            rolls |> List.skip (l0v - l2v) |> List.sum

    and ldice = function
        | L_Dice.Next loop_val -> loop loop_val
        | L_Dice.A l0 ->
            let l0v = loop l0
            if l0v < 1 then failwith "Can't roll a die with fewer than one side"
            let roll = System.Random().Next(1, l0v + 1)
            if depth <= dlim then rolls_agg <- rolls_agg + $"d%d{l0v}l: %i{roll}\n"
            roll
        | L_Dice.B (l0, l1) ->
            let l0v, l1v = loop l0, loop l1
            if l0v < 1 then failwith "Can't roll fewer than one die"
            if l1v < 1 then failwith "Can't roll a die with fewer than one side"
            let rolls = List.init l0v (fun _ -> System.Random().Next(1, l1v + 1)) |> List.sort
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}d%d{l1v}l: %s{los rolls}\n"
            rolls |> List.head
        | L_Dice.C (l0, l1) ->
            let l0v, l1v = loop l0, loop l1
            if l0v < 1 then failwith "Can't roll a die with fewer than one side"
            if l1v < 1 then failwith "Can't keep fewer than one die"
            let rolls = List.init 1 (fun _ -> System.Random().Next(1, l0v + 1))
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}l%d{l1v}: %s{los rolls}\n"
            rolls |> List.take l1v |> List.sum
        | L_Dice.D (l0, l1, l2) ->
            let l0v, l1v, l2v = loop l0, loop l1, loop l2
            if l0v < 1 then failwith "Can't roll fewer than one die"
            if l1v < 1 then failwith "Can't roll a die with fewer than one side"
            if l2v < 1 then failwith "Can't keep fewer than one die"
            let rolls = List.init l0v (fun _ -> System.Random().Next(1, l1v + 1)) |> List.sort
            if depth <= dlim then rolls_agg <- rolls_agg + $"%d{l0v}d%d{l1v}l%d{l2v}: %s{los rolls}\n"
            rolls |> List.take l2v |> List.sum

    and loop = function
        | Num i -> i
        | Closed rpg0_val ->
            depth <- depth + 1
            let a = rpg0 rpg0_val
            depth <- depth - 1
            a

    let result = rpg0 p
    $"%s{rolls_agg}**%d{result}**", result
