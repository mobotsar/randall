﻿module randall.Robot

open System
open System.Threading.Tasks
open DSharpPlus.CommandsNext
open DSharpPlus.CommandsNext.Attributes

type BardBot () =

    inherit BaseCommandModule ()
    [<Command "r">]
    member this.rolling (ctx: CommandContext, [<ParamArray>] dice: string[]) =
        task {
            do!
                ref (Parser.lex (dice |> Array.fold (fun a e -> a + e) "")) |> Parser.parse |> Eval.eval |> fst
                |> sprintf "%s"
                |> ctx.RespondAsync
                |> Async.AwaitTask
                |> Async.Ignore
            }
        :> Task