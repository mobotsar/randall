﻿module randall.Parser

type Token = AddT | SubT | MulT | DivT | ExpT | DT | GT | LT | OpT | CpT | NumT of int

let rec lex s =
    if String.length s = 0 then [] else
    match s[0] with
    | '+' -> AddT :: lex s[1..]
    | '-' -> SubT :: lex s[1..]
    | '*'
    | 'x' -> MulT :: lex s[1..]
    | '/' -> DivT :: lex s[1..]
    | '^' -> ExpT :: lex s[1..]
    | 'd' -> DT :: lex s[1..]
    | 'g' -> GT :: lex s[1..]
    | 'l' -> LT :: lex s[1..]
    | '(' -> OpT :: lex s[1..]
    | ')' -> CpT :: lex s[1..]
    | d when d > '/' && d < ':' -> tokenize_num s
    | c when c < '!' -> lex s[1..]
    | c -> failwith ("Illegal character: " + System.Char.ToString c)
and tokenize_num s =
    let rec select (s : string) n len =
        if len = 0 then n else
        if s[0] > '/' && s[0] < ':' then
            select s[1..] (n + 1) (len - 1)
        else n
    in
    let fin = select s 0 (String.length s) in
    NumT (int s[..fin - 1]) :: lex s[fin..]
    

// This tree type and the associated approach of building one at parse time are not needed.
// Indeed, they're detrimental to the complexity and performance of the application.
// However, I like the tree - it's cool - and so I will be building and evaluating one.
type Rpg0  = Single of Rpg1 * Rpg01
and Rpg01  = Add of Rpg1 * Rpg01
           | Sub of Rpg1 * Rpg01
           | Nil
and Rpg1   = Single of Rpg2 * Rpg11
and Rpg11  = Mul of Rpg2 * Rpg11
           | Div of Rpg2 * Rpg11
           | Nil
and Rpg2   = Single of All * Rpg21
and Rpg21  = Exp of All * Rpg21
           | Nil
and All    = A of Loop
           | B of Loop * Loop
           | Next of G_Dice
and G_Dice = A of Loop
           | B of Loop * Loop
           | C of Loop * Loop
           | D of Loop * Loop * Loop
           | Next of L_Dice
and L_Dice = A of Loop
           | B of Loop * Loop
           | C of Loop * Loop
           | D of Loop * Loop * Loop
           | Next of Loop
and Loop   = Num of int
           | Closed of Rpg0

let match_one (t : 'a list ref) (v : 'a) =
    if t.Value.IsEmpty then failwith "Input ended unexpectedly" else
    if t.Value.Head = v then t.Value <- t.Value.Tail
    else failwith ("Expected " + v.ToString() + " but got " + t.Value.Head.ToString())

let eat_one (t : Token list ref) = t.Value <- t.Value.Tail

let peek_opt (t : Token list ref) = List.tryHead t.Value

let dice_follow = [|CpT; AddT; SubT; MulT; DivT; ExpT|]


// target and follow must be disjoint
let rec decide t target follow depth =
    if List.isEmpty t then None else
    let hd = List.head t
    let od = depth
    let depth = (if hd = OpT then depth + 1 else (if hd = CpT then depth - 1 else depth))
    if depth <> 0 then decide (List.tail t) target follow depth else
    if Array.contains hd target && od = depth then Some(hd) else
    if Array.contains hd follow && od = depth then Some(hd) else
    decide (List.tail t) target follow depth


let rec rpg0 t =
    let a = rpg1 t
    Rpg0.Single (a, rpg01 t)
    
and rpg01 t =
    match peek_opt t with
    | None | Some(CpT) -> Rpg01.Nil
    | Some(tok) ->
        match tok with
        | AddT -> eat_one t; let a = rpg1 t in Add(a, rpg01 t)
        | SubT -> eat_one t; let a = rpg1 t in Sub(a, rpg01 t)
        | _ -> failwith ("Found unexpected token: " + tok.ToString())

and rpg1 t =
    let a = rpg2 t
    Rpg1.Single (a, rpg11 t)
    
and rpg11 t =
    match peek_opt t with
    | None -> Rpg11.Nil
    | Some(tok) ->
        match tok with
        | CpT | AddT | SubT -> Rpg11.Nil
        | MulT -> eat_one t; let a = rpg2 t in Mul(a, rpg11 t)
        | DivT -> eat_one t; let a = rpg2 t in Div(a, rpg11 t)
        | _ -> failwith ("Found unexpected token: " + tok.ToString())

and rpg2 t =
    let a = all t
    Rpg2.Single (a, rpg21 t)
    
and rpg21 t =
    match peek_opt t with
    | None -> Rpg21.Nil
    | Some(tok) ->
        match tok with
        | CpT | AddT | SubT | MulT | DivT -> Rpg21.Nil
        | ExpT -> eat_one t; let a = all t in Exp(a, rpg21 t)
        | _ -> failwith ("Found unexpected token: " + tok.ToString())

and all t =
    let factor = decide t.Value [|DT|] dice_follow 0
    if factor <> Some DT then All.Next(G_Dice.Next(L_Dice.Next(loop t))) else
    let factor = decide t.Value [|GT; LT|] dice_follow 0
    match factor with
    | Some GT -> All.Next(g_dice t)
    | Some LT -> All.Next(G_Dice.Next(l_dice t))
    | _ ->
        match peek_opt t with
        | None -> failwith "Input ended unexpectedly"
        | Some DT -> eat_one t; All.A(loop t)
        | Some (NumT _) | Some OpT -> let a = loop t in match_one t DT; All.B(a, loop t)
        | Some tok -> failwith ("Found unexpected token: " + tok.ToString())

and g_dice t =
    match peek_opt t with
    | None -> failwith "Input ended unexpectedly"
    | Some tok ->
        match tok with
        | OpT | NumT _ ->
            let a = loop t in eat_one t
            let b = loop t in eat_one t
            match peek_opt t with
            | None -> G_Dice.B (a, b)
            | Some tok when Array.contains tok dice_follow -> G_Dice.B (a, b)
            | Some OpT | Some (NumT _) -> G_Dice.D (a, b, loop t)
            | Some tok -> failwith ("Found unexpected token: " + tok.ToString())
        | DT ->
            eat_one t
            let a = loop t in eat_one t
            match peek_opt t with
            | None -> G_Dice.A a
            | Some tok when Array.contains tok dice_follow -> G_Dice.A a
            | Some OpT | Some (NumT _) -> G_Dice.C (a, loop t)
            | Some tok -> failwith ("Found unexpected token: " + tok.ToString())
        | _ -> failwith ("Found unexpected token: " + tok.ToString())

and l_dice t =
    match peek_opt t with
    | None -> failwith "Input ended unexpectedly"
    | Some tok ->
        match tok with
        | OpT | NumT _ ->
            let a = loop t in eat_one t
            let b = loop t in eat_one t
            match peek_opt t with
            | None -> L_Dice.B (a, b)
            | Some tok when Array.contains tok dice_follow -> L_Dice.B (a, b)
            | Some OpT | Some (NumT _) -> L_Dice.D (a, b, loop t)
            | Some tok -> failwith ("Found unexpected token: " + tok.ToString())
        | DT ->
            eat_one t
            let a = loop t in eat_one t
            match peek_opt t with
            | None -> L_Dice.A a
            | Some tok when Array.contains tok dice_follow -> L_Dice.A a
            | Some OpT | Some (NumT _) -> L_Dice.C (a, loop t)
            | Some tok -> failwith ("Found unexpected token: " + tok.ToString())
        | _ -> failwith ("Found unexpected token: " + tok.ToString())

and loop t =
    match peek_opt t with
    | None -> failwith "Input ended unexpectedly"
    | Some(NumT n) -> eat_one t; Num n
    | _ ->
        match_one t OpT
        let a = rpg0 t
        match_one t CpT
        Closed a

let parse t =
    let p = rpg0 t
    if t.Value.Length <> 0 then failwith $"Found unexpected token: %A{t.Value.Head}"
    p
